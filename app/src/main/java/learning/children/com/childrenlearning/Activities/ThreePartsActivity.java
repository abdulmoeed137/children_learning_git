package learning.children.com.childrenlearning.Activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.transitionseverywhere.TransitionManager;

import learning.children.com.childrenlearning.BaseActivity;
import learning.children.com.childrenlearning.Model.FirstModel;
import learning.children.com.childrenlearning.R;


public class ThreePartsActivity extends BaseActivity {

    TextView heading;
    ImageView empty1,empty2,empty3,puzzle1,puzzle2,puzzle3;
    boolean isPuzzle1 = false,isPuzzle2=false,isPuzzle3 = false;
    boolean isComplete1 = false,isComplete2=false,isComplete3 = false;

    LinearLayout t_container;

    ImageView gifImageView;

    FirstModel model ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initialize();
        setUpComponents();
    }

    @Override
    protected int getLayout() {

        int resId = getIntent().getExtras().getInt("resId");

        int empty1 = getIntent().getExtras().getInt("empty1");
        int empty2 = getIntent().getExtras().getInt("empty2");
        int empty3 = getIntent().getExtras().getInt("empty3");

        int img1 =getIntent().getExtras().getInt("img1");
        int img2 =getIntent().getExtras().getInt("img2");
        int img3 =getIntent().getExtras().getInt("img3");

        String displayName = getIntent().getExtras().getString("displayName");
        model = new FirstModel(displayName,resId,empty1,empty2,empty3,img1,img2,img3);
        return  model.getResId();
    }


    private void setUpComponents() {

        ((TextView)findViewById(R.id.heading)).setText(model.getDisplayName());

        
        empty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(t_container);
                if (!isPuzzle1 && !isPuzzle2 && !isPuzzle3){
                  //  Toast.makeText(CamelLevelActivity.this, "Please Select Puzzle", Toast.LENGTH_SHORT).show();
                }
                else {
                if (isPuzzle1)
                {
                    puzzle1.setBackgroundColor(Color.TRANSPARENT);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        puzzle1.setImageDrawable(getResources().getDrawable(model.getEmpty1(), getApplicationContext().getTheme()));
                        empty1.setBackground(getResources().getDrawable(model.getImg1(), getApplicationContext().getTheme()));

                    } else {
                        puzzle1.setImageDrawable(getResources().getDrawable(model.getEmpty1()));
                        empty1.setBackground(getResources().getDrawable(model.getImg1()));
                    }

                 //   Toast.makeText(CamelLevelActivity.this, "Well Done", Toast.LENGTH_SHORT).show();
                    empty1.setEnabled(false);
                    isPuzzle1=false;
                    puzzle1.setEnabled(false);
                    isComplete1 = true;
                    if (isComplete1 && isComplete2 && isComplete3)
                    {
                        hidePuzzle();
                        Vibrator w = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 500 milliseconds
                        w.vibrate(500);
                      //  Toast.makeText(CamelLevelActivity.this, "Excellent Bone Complete", Toast.LENGTH_SHORT).show();

                        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);
                        Glide.with(ThreePartsActivity.this).load(R.drawable.newgif).into(imageViewTarget);
                    }
                }
                else 
                {
                   // Toast.makeText(CamelLevelActivity.this, "Wrong Puzzle. Try Again. You can do this", Toast.LENGTH_SHORT).show();
                }
                }
            }
        });


        empty2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(t_container);
                if (!isPuzzle1 && !isPuzzle2 && !isPuzzle3){
                    Toast.makeText(ThreePartsActivity.this, "Please Select Puzzle", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (isPuzzle2)
                    {
                        puzzle2.setBackgroundColor(Color.TRANSPARENT);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            puzzle2.setImageDrawable(getResources().getDrawable(model.getEmpty2(), getApplicationContext().getTheme()));
                            empty2.setBackground(getResources().getDrawable(model.getImg2(), getApplicationContext().getTheme()));

                        } else {
                            puzzle2.setImageDrawable(getResources().getDrawable(model.getEmpty2()));
                            empty2.setBackground(getResources().getDrawable(model.getImg2()));
                        }

                      //  Toast.makeText(CamelLevelActivity.this, "Well Done", Toast.LENGTH_SHORT).show();
                        empty2.setEnabled(false);
                        isPuzzle2=false;
                        puzzle2.setEnabled(false);
                        isComplete2 = true;
                        if (isComplete1 && isComplete2 && isComplete3)
                        {
                            hidePuzzle();
                            Vibrator w = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            // Vibrate for 500 milliseconds
                            w.vibrate(500);
  //                          Toast.makeText(CamelLevelActivity.this, "Excellent Bone Complete", Toast.LENGTH_SHORT).show();

                            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);
                            Glide.with(ThreePartsActivity.this).load(R.drawable.newgif).into(imageViewTarget);
                        }
                    }
                    else
                    {
//                        Toast.makeText(CamelLevelActivity.this, "Wrong Puzzle. Try Again. You can do this", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



        empty3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(t_container);
                if (!isPuzzle1 && !isPuzzle2 && !isPuzzle3){
                  //  Toast.makeText(CamelLevelActivity.this, "Please Select Puzzle", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (isPuzzle3)
                    {
                        puzzle3.setBackgroundColor(Color.TRANSPARENT);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            puzzle3.setImageDrawable(getResources().getDrawable(model.getEmpty3(), getApplicationContext().getTheme()));
                            empty3.setBackground(getResources().getDrawable(model.getImg3(), getApplicationContext().getTheme()));

                        } else {
                            puzzle3.setImageDrawable(getResources().getDrawable(model.getEmpty3()));
                            empty3.setBackground(getResources().getDrawable(model.getImg3()));
                        }

                     //   Toast.makeText(CamelLevelActivity.this, "Well Done", Toast.LENGTH_SHORT).show();
                        empty3.setEnabled(false);
                        isPuzzle3=false;
                        puzzle3.setEnabled(false);
                        isComplete3 = true;
                        if (isComplete1 && isComplete2 && isComplete3)
                        {
                            hidePuzzle();
                            Vibrator w = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            // Vibrate for 500 milliseconds
                            w.vibrate(500);
                       //     Toast.makeText(CamelLevelActivity.this, "Excellent Gone Complete", Toast.LENGTH_SHORT).show();

                            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(gifImageView);
                            Glide.with(ThreePartsActivity.this).load(R.drawable.newgif).into(imageViewTarget);
                        }
                    }
                    else
                    {
//                        Toast.makeText(CamelLevelActivity.this, "Wrong Puzzle. Try Again. You can do this", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        puzzle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzle1.setBackgroundColor(Color.BLUE);
                puzzle2.setBackgroundColor(Color.TRANSPARENT);
                puzzle3.setBackgroundColor(Color.TRANSPARENT);
                
                isPuzzle1 = true;
                isPuzzle2 = false;
                isPuzzle3 = false;
            }
        });

        puzzle2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzle1.setBackgroundColor(Color.TRANSPARENT);
                puzzle2.setBackgroundColor(Color.BLUE);
                puzzle3.setBackgroundColor(Color.TRANSPARENT);

                isPuzzle1 = false;
                isPuzzle2 = true;
                isPuzzle3 = false;
            }
        });

        puzzle3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzle1.setBackgroundColor(Color.TRANSPARENT);
                puzzle2.setBackgroundColor(Color.TRANSPARENT);
                puzzle3.setBackgroundColor(Color.BLUE);

                isPuzzle1 = false;
                isPuzzle2 = false;
                isPuzzle3 = true;
            }
        });

//


         }


         void hidePuzzle(){
        puzzle1.setVisibility(View.GONE);
             puzzle2.setVisibility(View.GONE);
             puzzle3.setVisibility(View.GONE);
         }

    private void initialize() {

        empty1 = (ImageView)findViewById(R.id.empty_one);
        empty2 = (ImageView)findViewById(R.id.empty_two);
        empty3 = (ImageView)findViewById(R.id.empty_three);

        puzzle1 = (ImageView)findViewById(R.id.puzzle_one);
        puzzle2 = (ImageView)findViewById(R.id.puzzle_two);
        puzzle3 = (ImageView)findViewById(R.id.puzzle_three);

        t_container = (LinearLayout)findViewById(R.id.t_container);

        gifImageView = (ImageView) findViewById(R.id.gif);
        gifImageView.bringToFront();
        gifImageView.setVisibility(View.VISIBLE);
    }


}
