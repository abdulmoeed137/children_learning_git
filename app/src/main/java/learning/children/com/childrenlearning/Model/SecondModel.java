package learning.children.com.childrenlearning.Model;

/**
 * Created by lenovo on 2/28/2018.
 */

public class SecondModel {
    String displayName;
    int resId, empty1,empty2,empty3,empty4,img1,img2,img3,img4;

    public SecondModel(String displayName, int resId, int empty1, int empty2, int empty3,int empty4, int img1, int img2, int img3,int img4) {
        this.displayName = displayName;
        this.resId = resId;
        this.empty1 = empty1;
        this.empty2 = empty2;
        this.empty3 = empty3;
        this.empty4=empty4;
        this.img1 = img1;
        this.img2 = img2;
        this.img3 = img3;
        this.img4= img4;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public int getEmpty1() {
        return empty1;
    }

    public void setEmpty1(int empty1) {
        this.empty1 = empty1;
    }

    public int getEmpty2() {
        return empty2;
    }

    public void setEmpty2(int empty2) {
        this.empty2 = empty2;
    }

    public int getEmpty3() {
        return empty3;
    }

    public void setEmpty3(int empty3) {
        this.empty3 = empty3;
    }

    public int getImg1() {
        return img1;
    }

    public void setImg1(int img1) {
        this.img1 = img1;
    }

    public int getImg2() {
        return img2;
    }

    public void setImg2(int img2) {
        this.img2 = img2;
    }

    public int getImg3() {
        return img3;
    }

    public void setImg3(int img3) {
        this.img3 = img3;
    }

    public int getEmpty4() {
        return empty4;
    }

    public void setEmpty4(int empty4) {
        this.empty4 = empty4;
    }

    public int getImg4() {
        return img4;
    }

    public void setImg4(int img4) {
        this.img4 = img4;
    }
}
