package learning.children.com.childrenlearning.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import learning.children.com.childrenlearning.Adapter.FourCutMenuAdapter;
import learning.children.com.childrenlearning.Adapter.MenuAdapter;
import learning.children.com.childrenlearning.Model.FirstModel;
import learning.children.com.childrenlearning.Model.SecondModel;
import learning.children.com.childrenlearning.R;

/**
 * Created by lenovo on 2/28/2018.
 */

public class FourCutMenuactivity extends AppCompatActivity {


    List<SecondModel> list = new ArrayList<>();
    ListView mListView;
    Context context;
    FourCutMenuAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu
        );


        initialize();setUpComponents();

    }

    private void setUpComponents() {
        setNewObject("owl");
        setNewObject("balon");
        setNewObject("band");
        setNewObject("brain");
        setNewObject("brush");
        setNewObject("carpet");
        setNewObject("caterpillar");
        setNewObject("cheese");
        setNewObject("chilli");
        setNewObject("class");
        setNewObject("clock");
        setNewObject("crow");
        setNewObject("dragon");
        setNewObject("drum");
        setNewObject("dryfruit");
        setNewObject("joker");
        setNewObject("kitchen");
        setNewObject("machine");
        setNewObject("medicine");
        setNewObject("popcorn");
        setNewObject("rocket");
        setNewObject("run");
        setNewObject("sparrow");
        setNewObject("telephone");
        setNewObject("tounge");
        setNewObject("train");
        setNewObject("tree");
        setNewObject("umberalla");
        setNewObject("watermelon");
        setNewObject("wheel");
        setNewObject("window");
        setNewObject("world");
        setNewObject("cucumber");
        setNewObject("deer");
        setNewObject("flower");
        setNewObject("home");
        setNewObject("joker");
        setNewObject("sheep");
        setNewObject("student");
        setNewObject("star");
        setNewObject("greenonion");
        setNewObject("camera");
        setNewObject("soldier");
        setNewObject("bee");
        setNewObject("capsule");
        setNewObject("mouse");
        setNewObject("gloves");
        setNewObject("quill");
        setNewObject("bat");
        setNewObject("balon");
        setNewObject("shirt");
        setNewObject("fishing");
        setNewObject("leg");
        setNewObject("chair");
        setNewObject("kangaroo");
        setNewObject("swing");
        setNewObject("jacket");
        setNewObject("home");
        setNewObject("unicorn");
        setNewObject("surprise");
        setNewObject("ant");
        setNewObject("pray");


    }

    private void initialize() {
        context = FourCutMenuactivity.this;
        mListView = (ListView)findViewById(R.id.mListView);
        adapter = new FourCutMenuAdapter(context,list);
        mListView.setAdapter(adapter);
    }

    public  int getResId(String resName, String dir) {
       return getResources().getIdentifier(resName, dir, getPackageName());

    }

    void setNewObject (String name )
    {
        list.add(new SecondModel(name,
              getResId("activity_level_v4_"+name,"layout"),
                getResId(name+"_empty1", "drawable"),
                getResId(name+"_empty2","drawable"),
                getResId(name+"_empty3", "drawable"),
                getResId(name+"_empty4", "drawable"),
                getResId(name+"_img1", "drawable"),
                getResId(name+"_img2", "drawable"),
                getResId(name+"_img3", "drawable"),
                getResId(name+"_img4", "drawable")
                ));
        adapter.notifyDataSetChanged();
    }

}
