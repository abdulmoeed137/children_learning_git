package learning.children.com.childrenlearning;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

import java.util.Locale;

public class Exercise2Activity extends AppCompatActivity {

    TextView heading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        initialize();
        setUpComponents();
    }

    private void setUpComponents() {

        AssetManager am = getApplicationContext().getAssets();

      Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ComicJensFreePro-Regular.ttf"));

        heading.setTypeface(typeface);

        String text = "<font color='red'>Exercise</font>";
        heading.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);



         }

    private void initialize() {
        heading = (TextView)this.findViewById(R.id.heading);
    }
}
