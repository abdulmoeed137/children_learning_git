package learning.children.com.childrenlearning;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class ExerciseActivity extends AppCompatActivity {

    TextView heading,word1,word2,word3,ans,confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);

        initialize();
        setUpComponents();
    }

    private void setUpComponents() {

        AssetManager am = getApplicationContext().getAssets();

      Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ComicJensFreePro-Regular.ttf"));

        heading.setTypeface(typeface);

        String text = "<font color='red'>Exercise</font>";
        heading.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        word1
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String s = word1.getText().toString();
                        ans.setText(s);
                    }
                });

        word2
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String s = word2.getText().toString();
                        ans.setText(s);
                    }
                });

        word3
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String s = word3.getText().toString();
                        ans.setText(s);
                    }
                });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String answere = ans.getText().toString();
                if (answere.equals("د"))
                {
                    Toast.makeText(ExerciseActivity.this, "Congratulation!! Right Answere", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(ExerciseActivity.this, "Wrong Answere", Toast.LENGTH_SHORT).show();
            }
        });
         }

    private void initialize() {
        heading = (TextView)this.findViewById(R.id.heading);

        word1 = (TextView)this.findViewById(R.id.word1);
        word2 = (TextView)this.findViewById(R.id.word2);
        word3 = (TextView)this.findViewById(R.id.word3);
        ans = (TextView)this.findViewById(R.id.ans);
        confirm  = (TextView)this.findViewById(R.id.confirm);

    }
}
