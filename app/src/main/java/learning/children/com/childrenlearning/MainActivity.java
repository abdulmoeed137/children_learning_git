package learning.children.com.childrenlearning;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    TextView heading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        setUpComponents();
    }

    private void setUpComponents() {

        AssetManager am = getApplicationContext().getAssets();

      Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ComicJensFreePro-Regular.ttf"));

        heading.setTypeface(typeface);

        String text = "<font color='red'>Arabic</font> <font color='blue'>Learning</font> " +
                "<font color='yellow'>Puzzle</font> <font color='white'>Game</font> " +
                "<font color='blue'>Using Eye tracker</font> <font color='blue'>And ChatBot For</font> " +
                "<font color='purple'>Learning</font> <font color='black'>Disability</font> ";
        heading.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,ChooseLevelActivity.class));
            }
        });


         }

    private void initialize() {
        heading = (TextView)this.findViewById(R.id.heading);
    }
}
