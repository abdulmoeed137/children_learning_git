package learning.children.com.childrenlearning;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class ChooseLetterVowelActivity extends AppCompatActivity {

    TextView heading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_letter_vowel);

        initialize();
        setUpComponents();
    }

    private void setUpComponents() {

        AssetManager am = getApplicationContext().getAssets();

      Typeface typeface = Typeface.createFromAsset(am,
                String.format(Locale.US, "fonts/%s", "ComicJensFreePro-Regular.ttf"));

        heading.setTypeface(typeface);

        String text = "<font color='red'>Choose</font> <font color='blue'>Letter </font>" +
                " <font color='blue'>Vowel</font>";
        heading.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        findViewById(R.id.btn_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChooseLetterVowelActivity.this,start_playing_activity.class));
            }
        });

         }

    private void initialize() {
        heading = (TextView)this.findViewById(R.id.heading);
    }
}
