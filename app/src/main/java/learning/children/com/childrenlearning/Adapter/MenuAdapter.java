package learning.children.com.childrenlearning.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import learning.children.com.childrenlearning.Holder.MenuHolder;
import learning.children.com.childrenlearning.Model.FirstModel;
import learning.children.com.childrenlearning.R;
import learning.children.com.childrenlearning.Activities.ThreePartsActivity;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by lenovo on 2/28/2018.
 */

public class MenuAdapter extends BaseAdapter {
    List<FirstModel> list = new ArrayList<>();
    LayoutInflater inflater;
    Context context;

    public MenuAdapter(Context context,List<FirstModel> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
        hasStableIds();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
      return  list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final MenuHolder holder;
        final FirstModel item = list.get(position);
        if ((convertView == null ))
        {
            holder = new MenuHolder();
            convertView  = inflater.inflate(R.layout.menu_item,null,false);
            holder.button = (Button)convertView.findViewById(R.id.button);
            convertView.setTag(holder);
        }else
            holder=(MenuHolder) convertView.getTag();
        holder.button.setText(item.getDisplayName());
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ThreePartsActivity.class);
                Log.d(TAG, "onClick: "+item.getResId());
               i.putExtra("resId",item.getResId());

               i.putExtra("empty1",item.getEmpty1());
                i.putExtra("empty2",item.getEmpty2());
                i.putExtra("empty3",item.getEmpty3());

                i.putExtra("img1",item.getImg1());
                i.putExtra("img2",item.getImg2());
                i.putExtra("img3",item.getImg3());

                i.putExtra("displayName",item.getDisplayName());
                context.startActivity(i);


            }
        });


        return convertView;
    }
}
