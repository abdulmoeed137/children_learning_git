package learning.children.com.childrenlearning.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import learning.children.com.childrenlearning.R;

/**
 * Created by lenovo on 2/28/2018.
 */

public class PreMenuActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_menu);

        findViewById(R.id.three).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PreMenuActivity.this,ThreeCutMenuactivity.class));
            }
        });

        findViewById(R.id.four).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PreMenuActivity.this,FourCutMenuactivity.class));
            }
        });

    }
}
