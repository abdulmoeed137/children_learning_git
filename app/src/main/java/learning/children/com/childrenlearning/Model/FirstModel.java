package learning.children.com.childrenlearning.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by lenovo on 2/28/2018.
 */

public class FirstModel {
    String displayName;
    int resId, empty1,empty2,empty3,img1,img2,img3;

    public FirstModel(String displayName, int resId, int empty1, int empty2, int empty3, int img1, int img2, int img3) {
        this.displayName = displayName;
        this.resId = resId;
        this.empty1 = empty1;
        this.empty2 = empty2;
        this.empty3 = empty3;
        this.img1 = img1;
        this.img2 = img2;
        this.img3 = img3;
    }


    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public int getEmpty1() {
        return empty1;
    }

    public void setEmpty1(int empty1) {
        this.empty1 = empty1;
    }

    public int getEmpty2() {
        return empty2;
    }

    public void setEmpty2(int empty2) {
        this.empty2 = empty2;
    }

    public int getEmpty3() {
        return empty3;
    }

    public void setEmpty3(int empty3) {
        this.empty3 = empty3;
    }

    public int getImg1() {
        return img1;
    }

    public void setImg1(int img1) {
        this.img1 = img1;
    }

    public int getImg2() {
        return img2;
    }

    public void setImg2(int img2) {
        this.img2 = img2;
    }

    public int getImg3() {
        return img3;
    }

    public void setImg3(int img3) {
        this.img3 = img3;
    }

}
