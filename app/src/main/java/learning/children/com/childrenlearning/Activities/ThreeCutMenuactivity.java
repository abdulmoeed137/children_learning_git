package learning.children.com.childrenlearning.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import learning.children.com.childrenlearning.Adapter.MenuAdapter;
import learning.children.com.childrenlearning.Model.FirstModel;
import learning.children.com.childrenlearning.R;

/**
 * Created by lenovo on 2/28/2018.
 */

public class ThreeCutMenuactivity extends AppCompatActivity {


    List<FirstModel> list = new ArrayList<>();
    ListView mListView;
    Context context;
    MenuAdapter adapter;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu
        );


        initialize();setUpComponents();

    }

    private void setUpComponents() {
        setNewObject("no name","almond");
        setNewObject("no name","bnana");
        setNewObject("no name","bone");
        setNewObject("no name","book");
        setNewObject("no name","cage");
        setNewObject("no name","camel");
        setNewObject("no name","cat");
        setNewObject("no name","chicken");
        setNewObject("no name","clay");
        setNewObject("no name","crown");
        setNewObject("no name","donkey");
        setNewObject("no name","elephant");
        setNewObject("no name","eye");
        setNewObject("no name","fish");
        setNewObject("no name","giraffee");
        setNewObject("no name","goal");
        setNewObject("no name","gold");
        setNewObject("no name","grapes");
        setNewObject("no name","heart");
        setNewObject("no name","honey");
        setNewObject("no name","house");
        setNewObject("no name","light");
        setNewObject("no name","lion");
        setNewObject("no name","meter");
        setNewObject("no name","mount");
        setNewObject("no name","nail");
        setNewObject("no name","onion");
        setNewObject("no name","ox");
        setNewObject("no name","paper");
        setNewObject("no name","perfume");
        setNewObject("no name","spocket");
        setNewObject("no name","stairs");
        setNewObject("no name","well");
        setNewObject("no name","wolf");
        setNewObject("no name","wood");
        setNewObject("أذن","ear" +
                "");

    }

    private void initialize() {
        context = ThreeCutMenuactivity.this;
        mListView = (ListView)findViewById(R.id.mListView);
        adapter = new MenuAdapter(context,list);
        mListView.setAdapter(adapter);
    }

    public  int getResId(String resName, String dir) {
       return getResources().getIdentifier(resName, dir, getPackageName());

    }

    void setNewObject (String displayName,String name )
    {
        list.add(new FirstModel(displayName,
              getResId("activity_level_"+name,"layout"),
                getResId(name+"_empty1", "drawable"),
                getResId(name+"_empty2","drawable"),
                getResId(name+"_empty3", "drawable"),
                getResId(name+"_img1", "drawable"),
                getResId(name+"_img2", "drawable"),
                getResId(name+"_img3", "drawable")));
        adapter.notifyDataSetChanged();
    }

}
